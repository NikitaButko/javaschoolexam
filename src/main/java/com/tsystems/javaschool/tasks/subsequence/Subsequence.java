package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        int currentIndex = 0;
        int previousIndex = -1;
        for (Object elem : x) {
            boolean elemIsFind = false;
            for (int j = currentIndex; j < y.size(); j++){
                if (elem.equals(y.get(j))) {
                    currentIndex = j;
                    if (currentIndex >= previousIndex) {
                        previousIndex = currentIndex;
                        elemIsFind = true;
                        break;
                    }
                }
            }
            if (!elemIsFind)
                return false;
        }
        return true;
    }
}
