package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
            if (!canBuildPyramid(inputNumbers))
                throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);
        int height = (int)pyramidHeight(inputNumbers.size());
        int width = height + height - 1;
        int[][] pyramid = new int[height][width];

        int numberIndex = inputNumbers.size() - 1;
        int jStep = 0;
        for (int i = height - 1; i >= 0; i--) {
            int iFlag = 0;
            for (int j = width - 1 - jStep; j >= 0; j -= 2) {
                pyramid[i][j] = inputNumbers.get(numberIndex--);
                iFlag++;
                if (iFlag == i + 1)
                    break;
            }
            jStep++;
        }
        return pyramid;
    }


    private static double pyramidHeight(int number) {
        return (Math.sqrt(1 + 8 * number) - 1) / 2;
    }
    

    private boolean canBuildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null))
            return false;

        return (pyramidHeight(inputNumbers.size())) % 1 == 0;
    }
	
}
