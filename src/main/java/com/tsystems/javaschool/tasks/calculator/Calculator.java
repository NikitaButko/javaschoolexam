package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

        public String evaluate(String statement) {

            if (statement == null)
                return null;

            if (statement.startsWith("-"))
                statement = statement.replaceFirst("-", "0-");
            if (statement.contains("(-"))
                statement = statement.replaceAll("\\(-", "(0-");
            statement = statement.replaceAll(" ", "");

            double rez;
            try {
                LinkedList<Double> operands = new LinkedList<>();
                LinkedList<Character> operators = new LinkedList<>();

                for (int i = 0; i < statement.length(); i++) {
                    char c = statement.charAt(i);

                    if (c == '(')
                        operators.add('(');
                    else if (c == ')') {
                        while (operators.getLast() != '(')
                            processOperator(operands, operators.removeLast());
                        operators.removeLast();
                    }
                    else if (isOperator(c)) {
                        while (!operators.isEmpty()
                                && priority(operators.getLast()) >= priority(c))
                            processOperator(operands, operators.removeLast());
                        operators.add(c);
                    }
                    else {
                        StringBuilder operand = new StringBuilder();
                        while (i < statement.length() && (Character.isDigit(statement.charAt(i))
                                || statement.charAt(i)=='.'))
                            operand.append(statement.charAt(i++));

                        --i;
                        operands.add(Double.parseDouble(operand.toString()));
                    }
                }

                while (!operators.isEmpty())
                    processOperator(operands, operators.removeLast());

                rez = operands.removeLast();
            }
            catch (NoSuchElementException | NumberFormatException | ArithmeticException e) { return null;}

            if (rez % 1 == 0)
                return (int)rez + "";
            else
                return Math.round(rez * 10000.0) / 10000.0 + "";
    }



    private static boolean isOperator(char operator) {
        return  operator == '+'
                || operator == '-'
                || operator == '*'
                || operator == '/';
    }

    private static int priority(char operator) {
        return  operator == '+' || operator == '-' ? 1 :
                operator == '*' || operator == '/' ? 2 :
                                                    -1;
    }

    private static void processOperator(LinkedList<Double> operands, char operator) {
        double right = operands.removeLast();
        double left = operands.removeLast();
        switch (operator) {
            case '+':
                operands.add(left + right);
                break;
            case '-':
                operands.add(left - right);
                break;
            case '*':
                operands.add(left * right);
                break;
            case '/':
                if (right == 0)
                    throw new ArithmeticException("/ by zero");
                operands.add(left / right);
                break;
        }

    }

}
